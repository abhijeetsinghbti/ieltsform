export const appConfig = {
  mainDomain: "https://crmleadedu.herokuapp.com",
};

export const middlePoints = "api/v1";

export const endPoints = {
  all_branches: `${middlePoints}/Settings/GetAllBranch`,
  all_countries: `${middlePoints}/Settings/GetAllCountry`,
  all_visa_type: `${middlePoints}/Settings/GetAllVisaTypes`,
  all_lead_source: `${middlePoints}/Settings/GetAllLeadSource`,
  all_qualifications: `${middlePoints}/Settings/GetAllQualification`,
};
