import React from 'react';
import {f, w, h} from '@utils/responsive';
import {View, Text, StyleSheet} from 'react-native';
class Header extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const {headerName} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.leftView}></View>
        <View style={styles.centerView}>
          <Text style={styles.headerText}>{headerName}</Text>
        </View>
        <View style={styles.rightVIew}></View>
      </View>
    );
  }
}

export default Header;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: h(8),
    borderBottomWidth: 1,
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    borderBottomColor: '#d3d3d3',
  },
  leftView: {
    flex: 1.5,
    width: '100%',
    height: '100%',
  },
  rightVIew: {
    flex: 1.5,
    width: '100%',
    height: '100%',
  },
  centerView: {
    flex: 7,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontWeight: '500',
    fontSize: f(1.6),
    color: '#000000',
  },
});
