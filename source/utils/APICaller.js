import axios from "axios";
import Events from "../utils/event";
import { appConfig } from "../../config";
const { mainDomain } = appConfig;

const APICaller = (endpoint, method, body, contentType) =>
  axios({
    url: `${mainDomain}/${endpoint}`,
    method: method || "GET",
    data: body,
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": contentType || "application/json",
    },

    responseType: "json",
  })
    .then((response) => {
      console.log(`response from ${mainDomain}/${endpoint} >> ${response}`);
      return response;
    })
    .catch((error) => {
      console.log(`Error from ${mainDomain}/${endpoint}>> ${error}`);
      Events.trigger("setSomethingWrongModal");

      throw error.response;
    });

export default APICaller;
