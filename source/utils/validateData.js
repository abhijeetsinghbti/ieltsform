export const validateList = (list) => {
  console.log("in coming list", list);

  if (list && list.length) {
    let results = [];
    for (let i = 0; i < list.length; i++) {
      let obj = {};
      obj["label"] = list[i]["name"];
      obj["value"] = list[i]["id"];
      results.push(obj);
    }
    return results;
  }
};

export const countriesList = (list) => {
  console.log("in coming countriesList", list);

  if (list && list.length) {
    let results = [];
    for (let i = 0; i < list.length; i++) {
      let obj = {};
      obj["label"] = list[i]["countryDescription"];
      obj["value"] = list[i]["id"];
      results.push(obj);
    }
    return results;
  }
};
