import { useAsyncStorage } from "@react-native-async-storage/async-storage";
import Event from "../utils/event";
const setUserDataInAsyncStorage = async (key, value) => {
  console.log("comon", key, value);
  const sKey = key.toString();
  const sValue = value.toString();
  const { getItem, setItem } = useAsyncStorage(sKey);
  try {
    await setItem(sValue);
    Event.trigger("GetUserDetail");
    console.log("saved");
  } catch (e) {
    console.log("common async storage error");
  }
};

export default setUserDataInAsyncStorage;
