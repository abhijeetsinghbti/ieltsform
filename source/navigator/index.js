import React from "react";
import { View } from "react-native";
import { AppStackNavigator, AuthStackNavigator } from "../screens";
import { NavigationContainer } from "@react-navigation/native";
import Events from "../utils/event";
import AsyncStorage from "@react-native-async-storage/async-storage";

class Source extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      userLogin: false,
    };
  }

  componentDidMount() {
    this.getUserDetail();
    Events.on("GetUserDetail", "gtu", () => {
      this.getUserDetail(true);
    });
  }

  getUserDetail = async (param) => {
    let userDetail = {};
    try {
      const keys = await AsyncStorage.getAllKeys();
      const result = await AsyncStorage.multiGet(keys);
      for (var i = 0; i < result.length; i++) {
        userDetail[result[i][0]] = result[i][1];
      }
      console.log("userDetail", userDetail);
      const { branchId } = userDetail;
      this.setState(
        {
          userLogin: branchId != "" ? true : false,
        },
        () => {
          console.log("userLogin", this.state.userLogin);
        }
      );

      //this.authenticateUser(param, userDetail);
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    const { userLogin } = this.state;

    return (
      <NavigationContainer>
        {userLogin ? <AppStackNavigator /> : <AuthStackNavigator />}
      </NavigationContainer>
    );
  }
}

export default Source;
