import React from "react";
import { f, w, h } from "@utils/responsive";
import Header from "../../components/header";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Platform,
} from "react-native";
import DownArrow from "../../utils/downArrow";
import RNPickerSelect from "react-native-picker-select";
import APICaller from "../../utils/APICaller";
import { endPoints } from "../../../config";
import { validateList } from "../../utils/validateData";
import Toast from "react-native-simple-toast";
import setUserDataInAsyncStorage from "../../utils/asyncStorage";
const { all_branches } = endPoints;
const pickerItems = [
  { label: "Yes", value: "yes" },
  { label: "No", value: "no" },
];
class SelectBranch extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      branchId: "",
      allBranches: [],
    };
  }

  componentDidMount() {
    this.getAllBranches();
  }

  getAllBranches = () => {
    const endpoint = all_branches;
    APICaller(endpoint)
      .then((response) => {
        console.log("response of get user interests", response);
        const { data } = response;
        const { data: list } = data;
        if (list && list.length) {
          const branches = validateList(list);
          console.log("branches", branches);
          this.setState({
            allBranches: branches,
          });
        }
      })
      .catch((error) => {
        console.log("error of get user interests", error);
      });
  };

  render() {
    const { branchId, allBranches } = this.state;
    return (
      <View style={styles.container}>
        <Header headerName={"Branch"} />
        <View style={styles.mainView}>
          <KeyboardAwareScrollView contentContainerStyle={styles.scrollView}>
            <View style={styles.inputsWrapper}>
              <Text style={styles.textView}>
                Please Select Your Branch to Continue.
              </Text>
            </View>
            <View style={styles.inputStyle}>
              <RNPickerSelect
                placeholder={{
                  label: "Select Branch",
                  value: " ",
                }}
                items={allBranches}
                value={branchId}
                style={pickerSelectStyles}
                pickerProps={{ mode: "dropdown" }}
                useNativeAndroidPickerStyle={false}
                onValueChange={(value, index) => {
                  this.setState({
                    branchId: value,
                  });
                }}
                Icon={() => {
                  return <DownArrow color="#000000" size={1.7} />;
                }}
              />
            </View>
            <View style={styles.addMoreView}>
              <TouchableOpacity
                onPress={() => {
                  console.log(branchId);
                  if (branchId != "") {
                    setUserDataInAsyncStorage("branchId", branchId);
                  } else {
                    Toast.show("Please select branch");
                  }
                }}
                style={styles.addMoreButton}
              >
                <Text>Submit</Text>
              </TouchableOpacity>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </View>
    );
  }
}

export default SelectBranch;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
  },
  mainView: {
    flex: 1,
    height: "100%",
    width: "100%",
  },
  scrollView: {
    width: "100%",
    paddingHorizontal: w(4),
  },
  inputsWrapper: {
    width: "100%",
    height: h(10),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  textView: {
    fontSize: f(1.5),
    marginLeft: 3,
  },
  addMoreView: {
    width: w(92),
    height: h(10),
    alignItems: "center",
    justifyContent: "center",
  },
  addMoreButton: {
    width: w(45),
    height: h(6),
    borderWidth: 1,
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#d3d3d3",
    backgroundColor: "#ffffff",
  },
  inputStyle: {
    borderWidth: 1,
    width: w(92),
    height: h(6),
    borderRadius: 10,
    borderColor: "#d3d3d3",
    backgroundColor: "#ffffff",
    justifyContent: "center",
    marginVertical: h(2),
    paddingHorizontal: w(2),
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: f(1.2),
    width: "100%",
    color: "#000000",
    height: "100%",
  },
  inputAndroid: {
    fontSize: f(1.2),
    height: "100%",
    color: "#000000",
  },
  iconContainer: {
    top: Platform.OS === "ios" ? 5 : 15,
    right: 5,
  },
  placeholder: {
    color: "#999999",
  },
});
