import React from "react";
import Images from "@assets";
import moment from "moment";
import { f, w, h } from "@utils/responsive";
import Header from "../../components/header";
import DateTimePicker from "@react-native-community/datetimepicker";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  View,
  Text,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Platform,
} from "react-native";
import DownArrow from "../../utils/downArrow";
import CheckBox from "@react-native-community/checkbox";
import RNPickerSelect from "react-native-picker-select";
import APICaller from "../../utils/APICaller";
import { endPoints } from "../../../config";
import { validateList, countriesList } from "../../utils/validateData";
import Toast from "react-native-simple-toast";

const { all_countries, all_visa_type, all_lead_source, all_qualifications } =
  endPoints;
const pickerItems = [
  { label: "Yes", value: "yes" },
  { label: "No", value: "no" },
];
//https://crmleadedu.herokuapp.com/api/v1/Settings/GetAllBranch
class IeltsForm extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      overAllScore: "",
      speakingScore: "",
      writingScore: "",
      readingScore: "",
      listeningScore: "",
      dateOfBirth: "",
      date: new Date(),
      sDate: new Date(),
      open: false,
      workExpId: "",
      isSelected: false,
      firstName: "",
      lastName: "",
      dob: "",
      email: "",
      phone: "",
      address: "",
      remarks: "",
      workDuration: "",
      passportNo: "",
      maritalStatus: "",
      allCountries: [],
      countryId: "",
      allLeads: [],
      leadId: "",
      allVisaTypes: [],
      allQualifications: [],
      qualificationInputs: [
        {
          date: new Date(),
          open: false,
          qualificationId: "",
          passingYear: "",
          percentage: "",
          uniBoard: "",
          description: "",
        },
      ],
      visaInfos: [
        {
          visaCountry: "",
          visaUniversity: "",
          visaCity: "",
          visaReason: "",
          visaRemarks: "",
          visaId: "",
        },
      ],
      visaRefusaleses: [
        { refVisaID: "", refCountry: "", refReason: "", refRemarks: "" },
      ],
      spouseQualifications: [
        {
          date: new Date(),
          open: false,
          qualificationId: 1,
          passingYear: 1,
          percentage: 100,
          description: "string",
        },
      ],
      sopen: false,
      spouseEmail: "",
      spouseFirstName: "",
      spouseLastName: "",
      spouseDob: "",
      spouseMobileNumber: "",
      spouseAddress: "",
      spouseRemarks: "",
      spouseWorkexperience: "",
      spouseWorkduration: "",
      spousePassportnumber: "",
    };
  }

  componentDidMount() {
    this.getAllCountries();
    this.getAllVisaTypes();
    this.getAllLeadSource();
    this.getAllQualifications();
  }

  addQuifications = () => {
    let qualificationInputs = [...this.state.qualificationInputs];
    const obj = {
      date: new Date(),
      open: false,
      qualificationId: "yes",
      passingYear: "",
      percentage: "",
      uniBoard: "",
      description: "",
    };
    if (qualificationInputs.length == 5) {
      console.log("you could not add more than 5 qualifications");
    } else {
      qualificationInputs.push(obj);
    }
    this.setState({
      qualificationInputs,
    });
  };

  removeQualifications = () => {
    let qualificationInputs = [...this.state.qualificationInputs];
    qualificationInputs.splice(qualificationInputs.length - 1);
    this.setState({
      qualificationInputs,
    });
  };

  addMoreVisaInfos = () => {
    let visaInfos = [...this.state.visaInfos];
    const obj = {
      visaType: "",
      visaCountry: "",
      visaUniversity: "",
      visaCity: "",
      visaReason: "",
      visaRemarks: "",
      visaId: "",
    };
    if (visaInfos.length == 5) {
      console.log("you could not add more than 5 qualifications");
    } else {
      visaInfos.push(obj);
    }
    this.setState({
      visaInfos,
    });
  };

  removeVisaInfos = () => {
    let visaInfos = [...this.state.visaInfos];
    visaInfos.splice(visaInfos.length - 1);
    this.setState({
      visaInfos,
    });
  };

  addRefUsaleses = () => {
    let visaRefusaleses = [...this.state.visaRefusaleses];
    const obj = {
      refVisaId: "",
      refCountry: "",
      refReason: "",
      refRemarks: "",
    };
    if (visaRefusaleses.length == 5) {
      console.log("you could not add more than 5 qualifications");
    } else {
      visaRefusaleses.push(obj);
    }
    this.setState({
      visaRefusaleses,
    });
  };

  addSpouseQualifications = () => {
    let spouseQualifications = [...this.state.spouseQualifications];
    const obj = {
      date: new Date(),
      open: false,
      qualificationId: 1,
      passingYear: 1,
      percentage: 100,
      description: "string",
    };
    if (spouseQualifications.length == 5) {
      console.log("you could not add more than 5 qualifications");
    } else {
      spouseQualifications.push(obj);
    }
    this.setState({
      spouseQualifications,
    });
  };

  removeSpouseQualifications = () => {
    let spouseQualifications = [...this.state.spouseQualifications];
    spouseQualifications.splice(spouseQualifications.length - 1);
    this.setState({
      spouseQualifications,
    });
  };

  removeRefUsaleses = () => {
    let visaRefusaleses = [...this.state.visaRefusaleses];
    visaRefusaleses.splice(visaRefusaleses.length - 1);
    this.setState({
      visaRefusaleses,
    });
  };

  getAllCountries = () => {
    const endpoint = all_countries;
    APICaller(endpoint)
      .then((response) => {
        console.log("response of countries", response);
        const { data } = response;
        const { data: list } = data;
        if (list && list.length) {
          const countries = countriesList(list);
          console.log("countries", countries);
          this.setState({
            allCountries: countries,
          });
        }
      })
      .catch((error) => {
        console.log("error of countries", error);
      });
  };

  getAllVisaTypes = () => {
    const endpoint = all_visa_type;
    APICaller(endpoint)
      .then((response) => {
        console.log("response of Visa type", response);
        const { data } = response;
        const { data: list } = data;
        if (list && list.length) {
          const visa = validateList(list);
          console.log("visa", visa);
          this.setState({
            allVisaTypes: visa,
          });
        }
      })
      .catch((error) => {
        console.log("error of Visa type", error);
      });
  };

  getAllLeadSource = () => {
    const endpoint = all_lead_source;
    APICaller(endpoint)
      .then((response) => {
        console.log("response of Lead Source", response);
        const { data } = response;
        const { data: list } = data;
        if (list && list.length) {
          const leadsource = validateList(list);
          console.log("leadsource", leadsource);
          this.setState({
            allLeads: leadsource,
          });
        }
      })
      .catch((error) => {
        console.log("error of Lead Source", error);
      });
  };

  getAllQualifications = () => {
    const endpoint = all_qualifications;
    APICaller(endpoint)
      .then((response) => {
        console.log("response of qualifications", response);
        const { data } = response;
        const { data: list } = data;
        if (list && list.length) {
          const qualifications = validateList(list);
          console.log("qualifications", qualifications);
          this.setState({
            allQualifications: qualifications,
          });
        }
      })
      .catch((error) => {
        console.log("error of qualifications", error);
      });
  };

  onSubmit = () => {
    const {
      visaInfos,
      date,
      open,
      dateOfBirth,
      workExpId,
      isSelected,
      visaRefusaleses,
      qualificationInputs,
      firstName,
      lastName,
      dob,
      email,
      phone,
      address,
      remarks,
      workDuration,
      passportNo,
      maritalStatus,
      allCountries,
      countryId,
      allLeads,
      readingScore,
      listeningScore,
      leadId,
      writingScore,
      allVisaTypes,
      speakingScore,
      overAllScore,
      allQualifications,

      spouseFirstName,
      spouseLastName,
      spouseDob,
      spouseMobileNumber,
      spouseAddress,
      spouseRemarks,
      spouseWorkexperience,
      spouseWorkduration,
      spousePassportnumber,
    } = this.state;
    let spouseQualifications = [this.state.spouseQualifications];
    let userQualifications = [...this.state.qualificationInputs];
    let createUserVisa = [];
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", "<API Key>");

    for (let i = 0; i < userQualifications.length; i++) {
      let obj = {};
      obj = userQualifications[i];
      delete obj["date"];
      delete obj["open"];
      delete obj["uniBoard"];
    }

    for (let i = 0; i < visaInfos.length; i++) {
      const {
        visaCountry,
        visaUniversity,
        visaCity,
        visaReason,
        visaRemarks,
        visaId,
      } = visaInfos[i];

      let obj = {};
      obj["visaTypeId"] = visaId;
      obj["countryId"] = visaCountry;
      obj["city"] = visaCity;
      obj["reason"] = visaReason;
      obj["remarks"] = visaRemarks;
      createUserVisa.push(obj);
    }

    for (let i = 0; i < spouseQualifications.length; i++) {
      let obj = {};
      obj = spouseQualifications[i];
      delete obj["date"];
      delete obj["open"];
    }

    var raw = JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      dob: dateOfBirth,
      emailAddress: email,
      mobileNumber: phone,
      address: address,
      remarks: remarks,
      workexperience: workExpId === "yes" ? true : false,
      workduration: workDuration,
      passportnumber: passportNo,
      branchId: 1,
      ismaritalstatus: maritalStatus,
      listeningScore: listeningScore,
      readingScore: readingScore,
      writingScore: writingScore,
      speakingScore: speakingScore,
      overAllScore: overAllScore,
      countryId: countryId,
      leadSourceId: leadId,
      leadsStatusId: 1,
      spouse: {
        firstName: spouseFirstName,
        lastName: spouseLastName,
        dob: spouseDob,
        mobileNumber: spouseMobileNumber,
        address: spouseAddress,
        remarks: spouseRemarks,
        workexperience: spouseWorkexperience === "yes" ? true : false,
        workduration: spouseWorkduration,
        passportnumber: spousePassportnumber,
        spouseQualifications: spouseQualifications,
      },
      userQualifications: userQualifications,
      createUserVisa: createUserVisa,
      visaRefusaleses: visaRefusaleses,
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://crmleadedu.herokuapp.com/api/v1/Leads", requestOptions)
      .then((response) => response.text())
      .then((result) => console.log(result))
      .catch((error) => console.log("error", error));
  };

  render() {
    const {
      date,
      open,
      sopen,
      sDate,
      workExpId,
      isSelected,
      visaInfos,
      visaRefusaleses,
      qualificationInputs,
      firstName,
      lastName,
      dob,
      email,
      phone,
      address,
      remarks,
      workDuration,
      passportNo,
      overAllScore,
      speakingScore,
      writingScore,
      readingScore,
      listeningScore,
      maritalStatus,
      allCountries,
      countryId,
      allLeads,
      leadId,
      allVisaTypes,
      allQualifications,
      spouseQualifications,
      spouseFirstName,
      spouseLastName,
      spouseDob,
      spouseEmail,
      spouseMobileNumber,
      spouseAddress,
      spouseRemarks,
      spouseWorkexperience,
      spouseWorkduration,
      spousePassportnumber,
    } = this.state;

    return (
      <View style={styles.container}>
        <Header headerName={"Ielts Form"} />
        <View style={styles.mainView}>
          <KeyboardAwareScrollView contentContainerStyle={styles.scrollView}>
            <View style={styles.inputsWrapper}>
              <TextInput
                placeholderTextColor={"#999999"}
                placeholder="First Name"
                value={firstName}
                style={styles.inputs}
                onChangeText={(value) => {
                  this.setState({
                    firstName: value,
                  });
                }}
              />
              <TextInput
                placeholderTextColor={"#999999"}
                placeholder="Last Name"
                style={styles.inputs}
                value={lastName}
                onChangeText={(value) => {
                  this.setState({
                    lastName: value,
                  });
                }}
              />
            </View>
            <View style={styles.inputsWrapper}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    open: true,
                  });
                }}
                style={styles.datePickerContainer}
              >
                {open ? (
                  <DateTimePicker
                    themeVariant={"dark"}
                    value={date}
                    mode={"date"}
                    onChange={(e, date) => {
                      console.log("Eeee", e, date);

                      const dob = moment(date).format("DD-MM-YYYY");
                      this.setState({
                        open: false,
                        dateOfBirth: dob,
                      });
                    }}
                  />
                ) : (
                  <View />
                )}
                <Text style={styles.placeholder}>DOB</Text>
                <Image
                  style={styles.caledarIcon}
                  resizeMode={"contain"}
                  source={Images.CalendarIcon}
                />
              </TouchableOpacity>
              <TextInput
                placeholderTextColor={"#999999"}
                placeholder="Email"
                style={styles.inputs}
                value={email}
                onChangeText={(value) => {
                  this.setState({
                    email: value,
                  });
                }}
              />
            </View>
            <View style={styles.inputsWrapper}>
              <TextInput
                keyboardType={"number-pad"}
                placeholderTextColor={"#999999"}
                placeholder="Mobile Number"
                style={styles.inputs}
                value={phone}
                onChangeText={(value) => {
                  this.setState({
                    phone: value,
                  });
                }}
              />
              <TextInput
                placeholderTextColor={"#999999"}
                placeholder="Address"
                style={styles.inputs}
                value={address}
                onChangeText={(value) => {
                  this.setState({
                    address: value,
                  });
                }}
              />
            </View>
            <View style={styles.inputWrapper}>
              <TextInput
                placeholderTextColor={"#999999"}
                placeholder="Remarks"
                style={styles.input}
                value={remarks}
                onChangeText={(value) => {
                  this.setState({
                    remarks: value,
                  });
                }}
              />
            </View>
            <View style={styles.inputStyle}>
              <RNPickerSelect
                placeholder={{
                  label: "Work Experience",
                  value: null,
                }}
                items={pickerItems}
                value={workExpId}
                style={pickerSelectStyles}
                pickerProps={{ mode: "dropdown" }}
                useNativeAndroidPickerStyle={false}
                onValueChange={(value, index) => {
                  this.setState({
                    workExpId: value,
                  });
                }}
                Icon={() => {
                  return <DownArrow color="#000000" size={1.7} />;
                }}
              />
            </View>
            {workExpId == "yes" ? (
              <View style={styles.inputWrapper}>
                <TextInput
                  placeholderTextColor={"#999999"}
                  placeholder="Work Duration"
                  style={styles.input}
                  value={workDuration}
                  onChangeText={(value) => {
                    this.setState({
                      workDuration: value,
                    });
                  }}
                />
              </View>
            ) : (
              <View />
            )}
            <View style={styles.inputsWrapper}>
              <TextInput
                placeholderTextColor={"#999999"}
                placeholder="Passport No."
                style={styles.inputs}
                value={passportNo}
                onChangeText={(value) => {
                  this.setState({
                    passportNo: value,
                  });
                }}
              />
              <TextInput
                placeholderTextColor={"#999999"}
                placeholder="Marital Status"
                style={styles.inputs}
                value={maritalStatus}
                onChangeText={(value) => {
                  this.setState({
                    maritalStatus: value,
                  });
                }}
              />
            </View>
            <View style={styles.inputStyle}>
              <RNPickerSelect
                placeholder={{
                  label: "Select Country",
                  value: null,
                }}
                items={allCountries}
                value={countryId}
                style={pickerSelectStyles}
                pickerProps={{ mode: "dropdown" }}
                useNativeAndroidPickerStyle={false}
                onValueChange={(value, index) => {
                  this.setState({
                    countryId: value,
                  });
                }}
                Icon={() => {
                  return <DownArrow color="#000000" size={1.7} />;
                }}
              />
            </View>
            <View style={styles.inputStyle}>
              <RNPickerSelect
                placeholder={{
                  label: "Select lead source",
                  value: null,
                }}
                items={allLeads}
                value={leadId}
                style={pickerSelectStyles}
                pickerProps={{ mode: "dropdown" }}
                useNativeAndroidPickerStyle={false}
                onValueChange={(value, index) => {
                  this.setState({
                    leadId: value,
                  });
                }}
                Icon={() => {
                  return <DownArrow color="#000000" size={1.7} />;
                }}
              />
            </View>
            <View style={styles.textWrapper}>
              <Text style={styles.textView}>Ielts Bands :</Text>
            </View>
            <View style={styles.inputsWrapper}>
              <TextInput
                placeholderTextColor={"#999999"}
                placeholder="Listening Score"
                keyboardType={"number-pad"}
                style={styles.inputs}
                value={listeningScore}
                onChangeText={(value) => {
                  this.setState({
                    listeningScore: value,
                  });
                }}
              />
              <TextInput
                placeholderTextColor={"#999999"}
                placeholder="Reading Score"
                keyboardType={"number-pad"}
                style={styles.inputs}
                value={readingScore}
                onChangeText={(value) => {
                  this.setState({
                    readingScore: value,
                  });
                }}
              />
            </View>
            <View style={styles.inputsWrapper}>
              <TextInput
                placeholderTextColor={"#999999"}
                placeholder="Writing Score"
                keyboardType={"number-pad"}
                style={styles.inputs}
                value={writingScore}
                onChangeText={(value) => {
                  this.setState({
                    writingScore: value,
                  });
                }}
              />
              <TextInput
                placeholderTextColor={"#999999"}
                placeholder="Speaking Score"
                keyboardType={"number-pad"}
                style={styles.inputs}
                value={speakingScore}
                onChangeText={(value) => {
                  this.setState({
                    speakingScore: value,
                  });
                }}
              />
            </View>
            <View style={styles.inputWrapper}>
              <TextInput
                placeholderTextColor={"#999999"}
                placeholder="Over All Score"
                keyboardType={"number-pad"}
                style={styles.input}
                value={overAllScore}
                onChangeText={(value) => {
                  this.setState({
                    overAllScore: value,
                  });
                }}
              />
            </View>

            <View style={styles.textWrapper}>
              <Text style={styles.textView}>Qualifications :</Text>
            </View>
            {qualificationInputs.map((data, index) => {
              return (
                <View key={index.toString()} style={styles.qualificationView}>
                  <View style={styles.inputStyle}>
                    <RNPickerSelect
                      placeholder={{
                        label: "Select Qualification",
                        value: null,
                      }}
                      items={allQualifications}
                      style={pickerSelectStyles}
                      pickerProps={{ mode: "dropdown" }}
                      useNativeAndroidPickerStyle={false}
                      onValueChange={(value, i) => {
                        if (value) {
                          let qualificationInputs = [];
                          qualificationInputs = [
                            ...this.state.qualificationInputs,
                          ];
                          qualificationInputs[index]["qualificationId"] = value;
                          this.setState({
                            qualificationInputs,
                          });
                        }
                      }}
                      Icon={() => {
                        return <DownArrow color="#000000" size={1.7} />;
                      }}
                    />
                  </View>
                  <View style={styles.inputsWrapper}>
                    <TouchableOpacity
                      onPress={() => {
                        let qualificationInputs = [];
                        qualificationInputs = [
                          ...this.state.qualificationInputs,
                        ];
                        qualificationInputs[index]["open"] = true;
                        this.setState({
                          qualificationInputs,
                        });
                      }}
                      style={styles.datePickerContainer}
                    >
                      {qualificationInputs[index]["open"] ? (
                        <DateTimePicker
                          themeVariant={"dark"}
                          value={qualificationInputs[index]["date"]}
                          mode={"date"}
                          onChange={(e, date) => {
                            let qualificationInputs = [];
                            qualificationInputs = [
                              ...this.state.qualificationInputs,
                            ];
                            qualificationInputs[index]["date"] = date;
                            qualificationInputs[index]["passingYear"] =
                              moment(date).format("DD-MM-YYYY");
                            qualificationInputs[index]["open"] = false;
                            this.setState({
                              qualificationInputs,
                            });
                          }}
                        />
                      ) : (
                        <View />
                      )}
                      <Text style={styles.placeholder}>
                        {qualificationInputs[index]["passingYear"]
                          ? qualificationInputs[index]["passingYear"]
                          : "Passing Year"}
                      </Text>
                      <Image
                        style={styles.caledarIcon}
                        resizeMode={"contain"}
                        source={Images.CalendarIcon}
                      />
                    </TouchableOpacity>
                    <TextInput
                      value={qualificationInputs[index]["percentage"]}
                      placeholderTextColor={"#999999"}
                      placeholder="Percentage"
                      style={styles.inputs}
                      onChangeText={(value) => {
                        let qualificationInputs = [];
                        qualificationInputs = [
                          ...this.state.qualificationInputs,
                        ];
                        qualificationInputs[index]["percentage"] = value;
                        this.setState({
                          qualificationInputs,
                        });
                      }}
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      value={data["uniBoard"]}
                      placeholderTextColor={"#999999"}
                      placeholder="University/Board"
                      style={styles.input}
                      onChangeText={(value) => {
                        let qualificationInputs = [];
                        qualificationInputs = [
                          ...this.state.qualificationInputs,
                        ];
                        qualificationInputs[index]["uniBoard"] = value;
                        this.setState({
                          qualificationInputs,
                        });
                      }}
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      value={data["description"]}
                      placeholderTextColor={"#999999"}
                      placeholder="Description"
                      style={styles.input}
                      onChangeText={(value) => {
                        let qualificationInputs = [];
                        qualificationInputs = [
                          ...this.state.qualificationInputs,
                        ];
                        qualificationInputs[index]["description"] = value;
                        this.setState({
                          qualificationInputs,
                        });
                      }}
                    />
                  </View>
                </View>
              );
            })}
            <View style={styles.addMoreView}>
              <TouchableOpacity
                onPress={() => {
                  this.addQuifications();
                }}
                style={styles.addMoreButton}
              >
                <Text>Add More</Text>
              </TouchableOpacity>
              {qualificationInputs.length > 1 ? (
                <TouchableOpacity
                  onPress={() => {
                    this.removeQualifications();
                  }}
                  style={styles.removeButton}
                >
                  <Text>Remove</Text>
                </TouchableOpacity>
              ) : (
                <View />
              )}
            </View>
            <View style={styles.textWrapper}>
              <Text style={styles.textView}>Visa Info :</Text>
            </View>

            {visaInfos.map((data, index) => {
              return (
                <View key={index.toString()} style={styles.qualificationView}>
                  <View style={styles.inputStyle}>
                    <RNPickerSelect
                      placeholder={{
                        label: "Select visa type",
                        value: null,
                      }}
                      items={allVisaTypes}
                      value={visaInfos[index]["visaId"]}
                      style={pickerSelectStyles}
                      pickerProps={{ mode: "dropdown" }}
                      useNativeAndroidPickerStyle={false}
                      onValueChange={(value) => {
                        let visaInfos = [];
                        visaInfos = [...this.state.visaInfos];
                        visaInfos[index]["visaId"] = value;
                        this.setState({
                          visaInfos,
                        });
                      }}
                      Icon={() => {
                        return <DownArrow color="#000000" size={1.7} />;
                      }}
                    />
                  </View>
                  <View style={styles.inputStyle}>
                    <RNPickerSelect
                      placeholder={{
                        label: "Select country",
                        value: null,
                      }}
                      items={pickerItems}
                      value={visaInfos[index]["visaCountry"]}
                      style={pickerSelectStyles}
                      pickerProps={{ mode: "dropdown" }}
                      useNativeAndroidPickerStyle={false}
                      onValueChange={(value) => {
                        let visaInfos = [];
                        visaInfos = [...this.state.visaInfos];
                        visaInfos[index]["visaCountry"] = value;
                        this.setState({
                          visaInfos,
                        });
                      }}
                      Icon={() => {
                        return <DownArrow color="#000000" size={1.7} />;
                      }}
                    />
                  </View>
                  <View style={styles.inputStyle}>
                    <RNPickerSelect
                      placeholder={{
                        label: "Select university",
                        value: null,
                      }}
                      items={pickerItems}
                      value={visaInfos[index]["visaUniversity"]}
                      style={pickerSelectStyles}
                      pickerProps={{ mode: "dropdown" }}
                      useNativeAndroidPickerStyle={false}
                      onValueChange={(value) => {
                        let visaInfos = [];
                        visaInfos = [...this.state.visaInfos];
                        visaInfos[index]["visaUniversity"] = value;
                        this.setState({
                          visaInfos,
                        });
                      }}
                      Icon={() => {
                        return <DownArrow color="#000000" size={1.7} />;
                      }}
                    />
                  </View>
                  <View style={styles.inputStyle}>
                    <RNPickerSelect
                      placeholder={{
                        label: "Select city",
                        value: null,
                      }}
                      items={pickerItems}
                      value={visaInfos[index]["visaCity"]}
                      style={pickerSelectStyles}
                      pickerProps={{ mode: "dropdown" }}
                      useNativeAndroidPickerStyle={false}
                      onValueChange={(value) => {
                        let visaInfos = [];
                        visaInfos = [...this.state.visaInfos];
                        visaInfos[index]["visaCity"] = value;
                        this.setState({
                          visaInfos,
                        });
                      }}
                      Icon={() => {
                        return <DownArrow color="#000000" size={1.7} />;
                      }}
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      value={visaInfos[index]["visaReason"]}
                      placeholderTextColor={"#999999"}
                      placeholder="Reason"
                      style={styles.input}
                      onChangeText={(value) => {
                        let visaInfos = [];
                        visaInfos = [...this.state.visaInfos];
                        visaInfos[index]["visaReason"] = value;
                        this.setState({
                          visaInfos,
                        });
                      }}
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      value={visaInfos[index]["visaRemarks"]}
                      placeholderTextColor={"#999999"}
                      placeholder="Remarks"
                      style={styles.input}
                      onChangeText={(value) => {
                        let visaInfos = [];
                        visaInfos = [...this.state.visaInfos];
                        visaInfos[index]["visaRemarks"] = value;
                        this.setState({
                          visaInfos,
                        });
                      }}
                    />
                  </View>
                </View>
              );
            })}

            <View style={styles.addMoreView}>
              <TouchableOpacity
                onPress={() => {
                  this.addMoreVisaInfos();
                }}
                style={styles.addMoreButton}
              >
                <Text>Add More</Text>
              </TouchableOpacity>
              {visaInfos.length > 1 ? (
                <TouchableOpacity
                  onPress={() => {
                    this.removeVisaInfos();
                  }}
                  style={styles.removeButton}
                >
                  <Text>Remove</Text>
                </TouchableOpacity>
              ) : (
                <View />
              )}
            </View>

            <View style={styles.textWrapper}>
              <Text style={styles.textView}>Visa Refusales :</Text>
            </View>
            {visaRefusaleses.map((data, index) => {
              return (
                <View style={styles.qualificationView}>
                  <View style={styles.inputStyle}>
                    <RNPickerSelect
                      placeholder={{
                        label: "Select visa type",
                        value: null,
                      }}
                      items={allVisaTypes}
                      value={visaRefusaleses[index]["refVisaId"]}
                      style={pickerSelectStyles}
                      pickerProps={{ mode: "dropdown" }}
                      useNativeAndroidPickerStyle={false}
                      onValueChange={(value) => {
                        let visaRefusaleses = [];
                        visaRefusaleses = [...this.state.visaRefusaleses];
                        visaRefusaleses[index]["refVisaId"] = value;
                        this.setState({
                          visaRefusaleses,
                        });
                      }}
                      Icon={() => {
                        return <DownArrow color="#000000" size={1.7} />;
                      }}
                    />
                  </View>
                  <View style={styles.inputStyle}>
                    <RNPickerSelect
                      placeholder={{
                        label: "Select country",
                        value: null,
                      }}
                      items={pickerItems}
                      value={visaRefusaleses[index]["refCountry"]}
                      style={pickerSelectStyles}
                      pickerProps={{ mode: "dropdown" }}
                      useNativeAndroidPickerStyle={false}
                      onValueChange={(value) => {
                        let visaRefusaleses = [];
                        visaRefusaleses = [...this.state.visaRefusaleses];
                        visaRefusaleses[index]["refCountry"] = value;
                        this.setState({
                          visaRefusaleses,
                        });
                      }}
                      Icon={() => {
                        return <DownArrow color="#000000" size={1.7} />;
                      }}
                    />
                  </View>

                  <View style={styles.inputWrapper}>
                    <TextInput
                      value={visaRefusaleses[index]["refReason"]}
                      placeholderTextColor={"#999999"}
                      placeholder="Reason"
                      style={styles.input}
                      onChangeText={(value) => {
                        let visaRefusaleses = [];
                        visaRefusaleses = [...this.state.visaRefusaleses];
                        visaRefusaleses[index]["refReason"] = value;
                        this.setState({
                          visaRefusaleses,
                        });
                      }}
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      value={visaRefusaleses[index]["refRemarks"]}
                      placeholderTextColor={"#999999"}
                      placeholder="Remarks"
                      style={styles.input}
                      onChangeText={(value) => {
                        let visaRefusaleses = [];
                        visaRefusaleses = [...this.state.visaRefusaleses];
                        visaRefusaleses[index]["refRemarks"] = value;
                        this.setState({
                          visaRefusaleses,
                        });
                      }}
                    />
                  </View>
                </View>
              );
            })}

            <View style={styles.addMoreView}>
              <TouchableOpacity
                onPress={() => {
                  this.addRefUsaleses();
                }}
                style={styles.addMoreButton}
              >
                <Text>Add More</Text>
              </TouchableOpacity>
              {visaRefusaleses.length > 1 ? (
                <TouchableOpacity
                  onPress={() => {
                    this.removeRefUsaleses();
                  }}
                  style={styles.removeButton}
                >
                  <Text>Remove</Text>
                </TouchableOpacity>
              ) : (
                <View />
              )}
            </View>

            <View style={styles.checkboxContainer}>
              <CheckBox
                value={isSelected}
                onValueChange={() => {
                  this.setState({
                    isSelected: !isSelected,
                  });
                }}
                style={styles.checkbox}
              />
              <Text style={styles.label}>
                Do you want to fill spouse information?
              </Text>
            </View>

            {isSelected ? (
              <View>
                <View style={styles.inputsWrapper}>
                  <TextInput
                    placeholderTextColor={"#999999"}
                    placeholder="First Name"
                    style={styles.inputs}
                    value={spouseFirstName}
                    onChangeText={(value) => {
                      this.setState({
                        spouseFirstName: value,
                      });
                    }}
                  />
                  <TextInput
                    placeholderTextColor={"#999999"}
                    placeholder="Last Name"
                    style={styles.inputs}
                    value={spouseLastName}
                    onChangeText={(value) => {
                      this.setState({
                        spouseLastName: value,
                      });
                    }}
                  />
                </View>
                <View style={styles.inputsWrapper}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        open: true,
                      });
                    }}
                    style={styles.datePickerContainer}
                  >
                    {sopen ? (
                      <DateTimePicker
                        themeVariant={"dark"}
                        value={sDate}
                        mode={"date"}
                        onChange={(e, date) => {
                          const dob = moment(date).format("DD-MM-YYYY");
                          this.setState({
                            open: false,
                            spouseDob: dob,
                            sDate: date,
                          });
                        }}
                      />
                    ) : (
                      <View />
                    )}
                    <Text style={styles.placeholder}>Select Date</Text>
                    <Image
                      style={styles.caledarIcon}
                      resizeMode={"contain"}
                      source={Images.CalendarIcon}
                    />
                  </TouchableOpacity>
                  <TextInput
                    placeholderTextColor={"#999999"}
                    placeholder="Email"
                    style={styles.inputs}
                    value={spouseEmail}
                    onChangeText={(value) => {
                      this.setState({
                        spouseEmail: value,
                      });
                    }}
                  />
                </View>
                <View style={styles.inputsWrapper}>
                  <TextInput
                    keyboardType={"number-pad"}
                    placeholderTextColor={"#999999"}
                    placeholder="Mobile Number"
                    style={styles.inputs}
                    value={spouseMobileNumber}
                    onChangeText={(value) => {
                      this.setState({
                        spouseMobileNumber: value,
                      });
                    }}
                  />
                  <TextInput
                    placeholderTextColor={"#999999"}
                    placeholder="Address"
                    style={styles.inputs}
                    value={spouseAddress}
                    onChangeText={(value) => {
                      this.setState({
                        spouseAddress: value,
                      });
                    }}
                  />
                </View>
                <View style={styles.inputWrapper}>
                  <TextInput
                    placeholderTextColor={"#999999"}
                    placeholder="Remarks"
                    style={styles.input}
                    value={spouseRemarks}
                    onChangeText={(value) => {
                      this.setState({
                        spouseRemarks: value,
                      });
                    }}
                  />
                </View>
                <View style={styles.inputStyle}>
                  <RNPickerSelect
                    placeholder={{
                      label: "Work Experience",
                      value: null,
                    }}
                    items={pickerItems}
                    value={spouseWorkexperience}
                    style={pickerSelectStyles}
                    pickerProps={{ mode: "dropdown" }}
                    useNativeAndroidPickerStyle={false}
                    onValueChange={(value, index) => {
                      this.setState({
                        spouseWorkexperience: value,
                      });
                    }}
                    Icon={() => {
                      return <DownArrow color="#000000" size={1.7} />;
                    }}
                  />
                </View>
                {spouseWorkexperience == "yes" ? (
                  <View style={styles.inputWrapper}>
                    <TextInput
                      placeholderTextColor={"#999999"}
                      placeholder="Work Duration"
                      style={styles.input}
                      value={spouseWorkduration}
                      onChangeText={(value) => {
                        this.setState({
                          spouseWorkduration: value,
                        });
                      }}
                    />
                  </View>
                ) : (
                  <View />
                )}
                <View style={styles.inputsWrapper}>
                  <TextInput
                    placeholderTextColor={"#999999"}
                    placeholder="Passport No."
                    style={styles.inputs}
                    value={spousePassportnumber}
                    onChangeText={(value) => {
                      this.setState({
                        spousePassportnumber: value,
                      });
                    }}
                  />
                </View>

                {spouseQualifications.map((data, index) => {
                  return (
                    <View
                      key={index.toString()}
                      style={styles.qualificationView}
                    >
                      <View style={styles.inputStyle}>
                        <RNPickerSelect
                          placeholder={{
                            label: "Select Qualification",
                            value: null,
                          }}
                          items={allQualifications}
                          style={pickerSelectStyles}
                          pickerProps={{ mode: "dropdown" }}
                          useNativeAndroidPickerStyle={false}
                          onValueChange={(value, i) => {
                            if (value) {
                              let spouseQualifications = [];
                              spouseQualifications = [
                                ...this.state.spouseQualifications,
                              ];
                              spouseQualifications[index]["qualificationId"] =
                                value;
                              this.setState({
                                spouseQualifications,
                              });
                            }
                          }}
                          Icon={() => {
                            return <DownArrow color="#000000" size={1.7} />;
                          }}
                        />
                      </View>
                      <View style={styles.inputsWrapper}>
                        <TouchableOpacity
                          onPress={() => {
                            let spouseQualifications = [];
                            spouseQualifications = [
                              ...this.state.spouseQualifications,
                            ];
                            spouseQualifications[index]["open"] = true;
                            this.setState({
                              spouseQualifications,
                            });
                          }}
                          style={styles.datePickerContainer}
                        >
                          {spouseQualifications[index]["open"] ? (
                            <DateTimePicker
                              themeVariant={"dark"}
                              value={spouseQualifications[index]["date"]}
                              mode={"date"}
                              onChange={(e, date) => {
                                let spouseQualifications = [];
                                spouseQualifications = [
                                  ...this.state.spouseQualifications,
                                ];
                                spouseQualifications[index]["date"] = date;
                                spouseQualifications[index]["passingYear"] =
                                  moment(date).format("DD-MM-YYYY");
                                spouseQualifications[index]["open"] = false;
                                this.setState({
                                  spouseQualifications,
                                });
                              }}
                            />
                          ) : (
                            <View />
                          )}
                          <Text style={styles.placeholder}>
                            {spouseQualifications[index]["passingYear"]
                              ? spouseQualifications[index]["passingYear"]
                              : "Passing Year"}
                          </Text>
                          <Image
                            style={styles.caledarIcon}
                            resizeMode={"contain"}
                            source={Images.CalendarIcon}
                          />
                        </TouchableOpacity>
                        <TextInput
                          value={spouseQualifications[index]["percentage"]}
                          placeholderTextColor={"#999999"}
                          placeholder="Percentage"
                          style={styles.inputs}
                          onChangeText={(value) => {
                            let spouseQualifications = [];
                            spouseQualifications = [
                              ...this.state.spouseQualifications,
                            ];
                            spouseQualifications[index]["percentage"] = value;
                            this.setState({
                              spouseQualifications,
                            });
                          }}
                        />
                      </View>

                      <View style={styles.inputWrapper}>
                        <TextInput
                          value={data["description"]}
                          placeholderTextColor={"#999999"}
                          placeholder="Description"
                          style={styles.input}
                          onChangeText={(value) => {
                            let spouseQualifications = [];
                            spouseQualifications = [
                              ...this.state.spouseQualifications,
                            ];
                            spouseQualifications[index]["description"] = value;
                            this.setState({
                              spouseQualifications,
                            });
                          }}
                        />
                      </View>
                    </View>
                  );
                })}
                <View style={styles.addMoreView}>
                  <TouchableOpacity
                    onPress={() => {
                      this.addSpouseQualifications();
                    }}
                    style={styles.addMoreButton}
                  >
                    <Text>Add More</Text>
                  </TouchableOpacity>
                  {spouseQualifications.length > 1 ? (
                    <TouchableOpacity
                      onPress={() => {
                        this.removeSpouseQualifications();
                      }}
                      style={styles.removeButton}
                    >
                      <Text>Remove</Text>
                    </TouchableOpacity>
                  ) : (
                    <View />
                  )}
                </View>
              </View>
            ) : (
              <View />
            )}
            <TouchableOpacity
              onPress={() => {
                this.onSubmit();
              }}
              style={styles.submitButton}
            >
              <Text>Submit</Text>
            </TouchableOpacity>
          </KeyboardAwareScrollView>
        </View>
      </View>
    );
  }
}

export default IeltsForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
  },
  mainView: {
    flex: 1,
    height: "100%",
    width: "100%",
  },
  scrollView: {
    width: "100%",
    paddingHorizontal: w(4),
  },
  inputsWrapper: {
    width: "100%",
    height: h(10),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  textWrapper: {
    width: "100%",
    height: h(3),
  },
  textView: {
    fontSize: f(1.5),
  },
  inputs: {
    width: w(44.2),
    height: h(6),
    borderWidth: 1,
    borderRadius: 10,
    paddingLeft: w(2),
    fontSize: f(1.2),
    color: "#000000",
    borderColor: "#d3d3d3",
    backgroundColor: "#ffffff",
  },
  input: {
    width: w(92),
    height: h(6),
    borderWidth: 1,
    borderRadius: 10,
    paddingLeft: w(2),
    fontSize: f(1.2),
    color: "#000000",
    borderColor: "#d3d3d3",
    backgroundColor: "#ffffff",
  },
  datePickerContainer: {
    width: w(44.2),
    height: h(6),
    borderWidth: 1,
    borderRadius: 10,
    paddingHorizontal: w(2),
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#d3d3d3",
    backgroundColor: "#ffffff",
  },
  addMoreView: {
    width: w(92),
    height: h(6),
    flexDirection: "row",
    paddingHorizontal: w(2),
    alignItems: "center",
    paddingHorizontal: w(10),
    justifyContent: "center",
  },
  addMoreButton: {
    width: w(35),
    height: h(6),
    borderWidth: 1,
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#d3d3d3",
    backgroundColor: "#ffffff",
  },
  submitButton: {
    width: w(90),
    height: h(6),
    borderWidth: 1,
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#d3d3d3",
    backgroundColor: "#ffffff",
    marginBottom: h(3),
  },
  removeButton: {
    width: w(35),
    height: h(6),
    borderWidth: 1,
    borderRadius: 10,
    marginLeft: w(5),
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#d3d3d3",
    backgroundColor: "#ffffff",
  },
  placeholder: {
    fontSize: f(1.2),
  },
  caledarIcon: {
    width: w(6),
    height: h(3),
    right: w(2),
    position: "absolute",
    tintColor: "#999999",
  },
  inputWrapper: {
    width: "100%",
    height: h(10),
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    width: "100%",
    height: h(10),
    alignItems: "center",
    justifyContent: "center",
  },
  inputStyle: {
    borderWidth: 1,
    width: w(92),
    height: h(6),
    borderRadius: 10,
    borderColor: "#d3d3d3",
    backgroundColor: "#ffffff",
    justifyContent: "center",
    marginVertical: h(2),
    paddingHorizontal: w(2),
  },
  qualificationView: {
    width: "100%",
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: f(1.2),
    width: "100%",
    color: "#000000",
    height: "100%",
  },
  inputAndroid: {
    fontSize: f(1.2),
    height: "100%",
    color: "#000000",
  },
  iconContainer: {
    top: Platform.OS === "ios" ? 4 : 12,
    right: 5,
  },
  headlessAndroidPicker: {
    width: w(102),
  },
  placeholder: {
    color: "#999999",
  },
});
