import React from 'react';
import IeltsForm from './ieltsFrom';
import SelectBranch from './selectBranch';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const screenOptionStyle = {
  headerShown: false,
};

function AppStackNavigator({navigation, route}) {
  return (
    <Stack.Navigator
      initialRouteName={'IeltsForm'}
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="IeltsForm" component={IeltsForm} />
    </Stack.Navigator>
  );
}

function AuthStackNavigator({navigation, route}) {
  return (
    <Stack.Navigator
      initialRouteName={'SelectBranch'}
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="SelectBranch" component={SelectBranch} />
    </Stack.Navigator>
  );
}

export {AuthStackNavigator, AppStackNavigator};
